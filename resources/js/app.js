import './bootstrap';
import { symbolSprite } from '@components/symbolSprite';
import { sleep } from '@helpers/utils';

symbolSprite('/images/symbol-sprite.html', 24);

// NOTE: Below is an example
sleep(3000).then(() => {
    console.log('sleep');
});

const obj = {
    a: 2,
};

console.log(obj?.b);
console.log(obj?.a);

if (document.querySelector('#swiper')) {
    import(/* webpackChunkName: "swiper" */ 'swiper').then(module => {
        console.log(module);
    });
}
