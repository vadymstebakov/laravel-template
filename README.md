## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Build setup

1. Install [NodeJS](https://nodejs.org/en/);
2. Install the NPM dependencies by running `npm install` or `yarn`.

## Usage

* `npm run dev` or `yarn run dev` runs your project in the development mode once;
* `npm run watch` or `yarn run watch` runs your project in the development mode on the local server;
* `npm run prod` or `yarn run prod` builds your project for production;
* `npm run hot` or `yarn run hot` runs your project in the hot module replacement;
* `npm run lint:js` or `yarn run lint:js` checks your JavaScript for errors and warnings;
* `npm run lint:scss` or `yarn run lint:scss` checks your SCSS for errors and warnings;
