const path = require('path');
const mix = require('laravel-mix');
const HTMLWebpackPlugin = require('html-webpack-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

const isProd = mix.inProduction();

const putSvgSprite = () => {
    return new HTMLWebpackPlugin({
        filename: 'images/symbol-sprite.html',
        template: './resources/images/sprites/symbol-sprite.html',
        inject: false,
        minify: {
            collapseWhitespace: isProd,
        },
    });
};

if (isProd) {
    mix.version();
}

mix.js('resources/js/app.js', 'public/js')
    .extract(['swiper'])
    .sass('resources/scss/app.scss', 'public/css')
    .sass('resources/scss/vendor.scss', 'public/css')
    .options({
        autoprefixer: {
            options: {
                browsers: ['>0.2%, not dead, not op_mini all'],
            },
        },
    })
    .browserSync({
        port: 8080,
        proxy: 'example-app',
    })
    .disableSuccessNotifications()
    .babelConfig({
        presets: [
            [
                '@babel/preset-env',
                {
                    targets: '>0.2%, not dead, not op_mini all',
                    useBuiltIns: 'usage',
                    corejs: 3,
                },
            ],
        ],
        plugins: [
            '@babel/transform-runtime',
            '@babel/plugin-syntax-dynamic-import',
            '@babel/plugin-proposal-optional-chaining',
        ],
    })
    .webpackConfig({
        plugins: [putSvgSprite()],
        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.(js)$/,
                    loader: 'eslint-loader',
                    include: /resources/,
                },
            ],
        },
        devtool: !isProd ? 'source-map' : false,
    })
    .sourceMaps(!isProd)
    .alias({
        '@': path.join(__dirname, 'resources/js'),
        '@components': path.join(__dirname, 'resources/js/components'),
        '@helpers': path.join(__dirname, 'resources/js/helpers'),
    });
